export default class CalcController {
    constructor() {
        this._locale = 'pt-br'
        this._currentDate;
        this._$displayCalc = document.querySelector('#display');
        this._$timeOnDisplay = document.querySelector('#hora');
        this._$dateOnDisplay = document.querySelector('#data');
        this.initialize();
    }
    initialize() {
        this._currentDate = this.currentDate;
        this._$displayCalc.innerHTML = this.displayCalc;
        this.setDisplayDateTime();
        setInterval(() => {
            this.setDisplayDateTime();
        }, 1000);

    }
    setDisplayDateTime() {
        this.displayTime = this.currentDate.toLocaleTimeString(this._locale);
        this.displayDate = this.currentDate.toLocaleDateString(this._locale, {
            day: '2-digit',
            month: 'long',
            year: '2-digit'
        });
    }
    get displayCalc() {
        return this._$displayCalc.innerHTML;
    }
    set displayCalc(value) {
        this._$displayCalc.innerHTML = value;
    }
    get displayTime() {
        return this._$timeOnDisplay.innerHTML;
    }
    get displayDate() {
        return this._$dateOnDisplay.innerHTML;
    }
    set displayTime(value) {
        this._$timeOnDisplay.innerHTML = value;
    }
    set displayDate(value) {
        this._$dateOnDisplay.innerHTML = value;
    }
    get currentDate() {
        return new Date();
    }
    set currentDate(value) {
        this._currentDate = value;
    }
}